import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";

import SourceSelect from "./components/SourceSelect";
import TimeSelect from "./components/TimeSelect";

import List from "./components/List";

import { fetchData } from "./store";

function Dashboard({ classes }) {
  const { publishedAt_lt, publishedAt_gt, source } = useSelector(
    (state) => state.dashboard.query
  );
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      dispatch(fetchData({ publishedAt_lt, publishedAt_gt, source }));
    })();
  }, [publishedAt_lt, publishedAt_gt, source]);

  return (
    <Paper>
      <Box className={classes.header}>
        <SourceSelect />
        <TimeSelect />
      </Box>
      <Box className={classes.main}>
        <Box>
          <List />
        </Box>
      </Box>
    </Paper>
  );
}

const style = {
  header: {
    display: "flex",
    minHeight: "85px",
    alignItems: "center",
  },
  main: {
    display: "flex",
  },
};

export default withStyles(style)(Dashboard);
