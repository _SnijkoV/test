import React from "react";
import { useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";


const style = {};

export default withStyles(style)(function TimeSelect({ classes }) {
  const data = useSelector((state) => state.dashboard.data);

  return (
    <ul>
      {data.map((item, index) => (
        <li key={index}>{item.title}</li>
      ))}
    </ul>
  );
});
