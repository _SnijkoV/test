import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { changeFilter } from "../store";

export default function SourceSelect() {
  const { source } = useSelector((state) => state.dashboard.query);
  const dispatch = useDispatch();

  const onChange = useCallback((e, value) => {
    if (value !== null) {
      dispatch(
        changeFilter({
          source: value,
        })
      );
    }
  }, []);

  return (
    <Autocomplete
      id="selectState"
      value={source}
      defaultValue={data[0]}
      options={data}
      onChange={onChange}
      style={{ width: 380, margin: "0 10px" }}
      disableClearable
      forcePopupIcon={false}
      renderInput={(params) => (
        <TextField
          {...params}
          size="small"
          variant="outlined"
          label="Size small"
          placeholder="Favorites"
        />
      )}
    />
  );
}

const data = [
  "Nasaspaceflight",
  "Spacex",
  "Spaceflightnow",
  "Space.com",
  "Spacenews",
  "Nasa",
  "Phys",
  "Arstechnica",
  "Blueorigin",
  "Spaceflightinsider",
  "Thejapantimes",
  "Theverge",
  "Teslarati",
  "Elonx",
  "Virgingalactic",
  "Esa",
];
