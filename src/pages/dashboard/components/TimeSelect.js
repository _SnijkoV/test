import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { format } from "date-fns";
import TextField from "@material-ui/core/TextField";
import DateRangePicker from "@material-ui/lab/DateRangePicker";
import AdapterDateFns from "@material-ui/lab/AdapterDateFns";
import LocalizationProvider from "@material-ui/lab/LocalizationProvider";
import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";

import { changeFilter } from "../store";

const style = {
  textField: {
    "& .MuiOutlinedInput-root": {
      height: "40px",
      padding: "0 10px",
    },
    margin: "23px 10px 0px"
  },
};

export default withStyles(style)(function TimeSelect({ classes }) {
  const { publishedAt_gt, publishedAt_lt } = useSelector(
    (state) => state.dashboard.query
  );
  const dispatch = useDispatch();

  const onChange = useCallback(([from, to]) => {
    if (from !== null && to !== null) {
      dispatch(
        changeFilter({
          publishedAt_gt: new Date(from).getTime(),
          publishedAt_lt: new Date(to).getTime(),
        })
      );
    }
  }, []);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DateRangePicker
        startText="Start date"
        endText="End date"
        format="yyyy-MM-dd"
        value={[publishedAt_gt, publishedAt_lt]}
        onChange={onChange}
        renderInput={(startProps, endProps) => (
          <>
            <TextField
              {...startProps}
              className={classes.textField}
              size="small"
              variant="outlined"
              label="Size small"
              placeholder="Favorites"
            />
            <Box sx={{ mx: 2 }}> to </Box>
            <TextField
              {...endProps}
              className={classes.textField}
              size="small"
              variant="outlined"
              label="Size small"
              placeholder="Favorites"
            />
          </>
        )}
      />
    </LocalizationProvider>
  );
});
