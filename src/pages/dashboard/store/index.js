import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const currentDate = new Date();
const currentDateMinus7Days = new Date(
  new Date().setDate(currentDate.getDate() - 7)
);

const initialState = {
  data: [],
  query: {
    source: "SpaceNews",
    publishedAt_lt: currentDate.getTime(),
    publishedAt_gt: currentDateMinus7Days.getTime(),
  },
};

const serialize = (obj) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
};

export const fetchData = createAsyncThunk(
  "dashboard/fetchData",
  async (params) => {
    const formatParams = {
      newsSite_contains: params.source,
      publishedAt_lt: new Date(params.publishedAt_lt).toISOString(),
      publishedAt_gt: new Date(params.publishedAt_gt).toISOString(),
    }; 

    const responce = await fetch(
      `https://api.spaceflightnewsapi.net/v3/articles?${serialize(formatParams)}`
    );

    const data = await responce.json();
    return data;
  }
);

export const counterSlice = createSlice({
  name: "dashboard",
  initialState,
  reducers: {
    changeFilter: (state, { payload }) => {
      state.query = { ...state.query, ...payload };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchData.pending, (state) => {
        state.loadDataStatus = "loading";
      })
      .addCase(fetchData.fulfilled, (state, action) => {
        state.loadDataStatus = "idle";
        state.data = action.payload;
      });
  },
});

export const { changeFilter } = counterSlice.actions;

export default counterSlice.reducer;
