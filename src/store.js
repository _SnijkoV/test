import { configureStore } from '@reduxjs/toolkit';
import dashboardReducer from "./pages/dashboard/store";

export const store = configureStore({
  reducer: {
    dashboard: dashboardReducer,
  },
});
